<?php

namespace SecretSales\Bundle\TwitterBundle\Exception;

use Exception;

/**
 * Class NoKeywordsException
 */
class NoKeywordsException extends Exception
{
}