<?php

namespace SecretSales\Bundle\TwitterBundle\Command;

use Exception;
use InvalidArgumentException;
use Knp\Bundle\LastTweetsBundle\Twitter\Exception\TwitterException;
use SecretSales\Bundle\TwitterBundle\Exception\NoKeywordsException;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class SyncCommand
 */
class KeywordsCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('secretsales:twitter:keywords')
            ->setDescription('Use this command to get the keyword count of the latest tweets.')
            ->addArgument('username', InputOption::VALUE_NONE, 'You need to specify the twitter username.')
            ->addOption('number', null, InputOption::VALUE_REQUIRED, 'How many tweets should be fetched?', null);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $username = $input->getArgument('username');
        $number = $input->getOption('number');

        $manager = $this->getContainer()->get('secret_sales_twitter.twitter_manager');

        if(is_string($number)) {
            if(!ctype_digit($number)) {
                throw new InvalidArgumentException('Number of tweets must be a number.');
            }
            $manager->setMaxNumberOfTweets(intval($number));
        }

        try {
            $keywords = $manager->fetch($username)->getKeywords();

            foreach($keywords as $keyword=>$count) {
                $output->writeln(sprintf('%s,%s', $keyword, $count));
            }
        }
        catch(TwitterException $e) {
            $output->writeln('<error>There was a problem while fetching tweets from the server:<error>');
            $output->writeln($e->getMessage());
            return 1;
        }
        catch(NoKeywordsException $e) {
            $output->writeln('<comment>No keywords for specified username.<comment>');
            return 1;
        }
        catch(Exception $e) {
            $output->writeln('<error>There was a problem:<error>');
            $output->writeln($e->getMessage());
            return 1;
        }

        return 0;
    }
}