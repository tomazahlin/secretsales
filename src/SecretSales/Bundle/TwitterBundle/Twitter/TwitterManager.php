<?php

namespace SecretSales\Bundle\TwitterBundle\Twitter;

use InvalidArgumentException;
use Knp\Bundle\LastTweetsBundle\Twitter\LastTweetsFetcher\FetcherInterface;

/**
 * Class TwitterManager
 */
class TwitterManager {

    /**
     * @var FetcherInterface
     */
    private $fetcher;

    /**
     * @var integer
     */
    private $maxNumberOfTweets;

    /**
     * @param FetcherInterface $fetcher
     * @param integer $maxNumberOfTweets
     */
    function __construct(FetcherInterface $fetcher, $maxNumberOfTweets)
    {
        $this->fetcher = $fetcher;
        $this->maxNumberOfTweets = $maxNumberOfTweets;
    }

    /**
     * With this method it is possible to override the default number of tweets fetched
     * @param integer $number
     * @return TwitterManager
     */
    public function setMaxNumberOfTweets($number)
    {
        $this->maxNumberOfTweets = (int)$number;
        return $this;
    }

    /**
     * This method uses Knp fetcher to fetch tweets for given username.
     * @param string $username
     * @throws InvalidArgumentException
     * @return TwitterResults
     */
    public function fetch($username)
    {
        if(strlen($username) === 0) {
            throw new InvalidArgumentException('Username cannot be blank');
        }
        return new TwitterResults($this->fetcher->fetch($username, $this->maxNumberOfTweets));
    }
}