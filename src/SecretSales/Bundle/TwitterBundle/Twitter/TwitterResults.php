<?php

namespace SecretSales\Bundle\TwitterBundle\Twitter;
use Knp\Bundle\LastTweetsBundle\Twitter\Tweet;
use SecretSales\Bundle\TwitterBundle\Exception\NoKeywordsException;

/**
 * Class TwitterResults
 */
class TwitterResults {

    /**
     * @var array
     */
    private $data = array();

    /**
     * @param array $tweets
     * @throws NoKeywordsException
     */
    function __construct(array $tweets)
    {
        if(count($tweets) === 0) {
            throw new NoKeywordsException();
        }

        $this->data = $tweets;
    }

    /**
     * @return array
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * This method returns an array of keywords, sorted by occurrence
     * @param int $sort (SORT_DESC, SORT_ASC)
     * @throws
     * @return array
     */
    public function getKeywords($sort = SORT_DESC)
    {
        $keywords = array();

        foreach($this->data as $data) {
            if($data instanceof Tweet) {
                $text = trim($data->getText());
                $newKeywords = explode(' ', $text);
                $keywords = array_merge($keywords, $this->cleanKeywords($newKeywords));
            }
        }

        $count = array_count_values($keywords);
        arsort($count,$sort);

        return $count;
    }

    /**
     * This method cleans keywords before they are merged
     * The ending punctuations are removed
     * In general, this method is not necessary, but the results are a little bit nicer that way
     * @param array $keywords
     * @return array
     */
    private function cleanKeywords(array $keywords)
    {
        $cleanKeywords = array();

        foreach($keywords as $keyword) {
            if(strlen($keyword) > 0) {
                $keyword = rtrim($keyword, ",.!?");
                $cleanKeywords[] = $keyword;
            }
        }

        return $cleanKeywords;
    }
}