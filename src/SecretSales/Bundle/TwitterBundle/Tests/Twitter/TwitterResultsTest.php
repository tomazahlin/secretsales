<?php

use Knp\Bundle\LastTweetsBundle\Twitter\Tweet;
use SecretSales\Bundle\TwitterBundle\Twitter\TwitterResults;

class TwitterResultsTest extends PHPUnit_Framework_TestCase
{
    /**
     * Tests that the keywords are correctly counted (TwitterResults)
     * @param $provider
     * @dataProvider getTweets
     * @group twitter
     */
    public function testKeywords($provider)
    {
        $results = new TwitterResults($provider['tweets']);

        $keywords = $results->getKeywords(SORT_DESC);

        reset($keywords);
        $first = key($keywords);

        $this->assertEquals($provider['keywords'][0], $first);

        for($i=0,$l=count($provider['keywords']); $i<$l; $i++) {
            $this->assertEquals($provider['counts'][$i], $keywords[$provider['keywords'][$i]]);
        }
    }

    /**
     * Tests the scenario when there are no tweets
     * @expectedException SecretSales\Bundle\TwitterBundle\Exception\NoKeywordsException
     * @group twitter
     */
    public function testNoKeywords()
    {
        new TwitterResults(array());
    }

    /**
     * Data provider to return an array of static test tweets
     * @return array
     */
    static function getTweets()
    {
        return array(
            array('data' => array(
                    'tweets' => array(
                        new Tweet(
                            array(
                                'id' => 1,
                                'username' => 'Test',
                                'text' => 'Some random text',
                                'created_at' => '2015-01-01'
                            )
                        ),
                        new Tweet(
                            array(
                                'id' => 2,
                                'username' => 'Test',
                                'text' => 'Another random tweet',
                                'created_at' => '2015-01-01'
                            )
                        ),
                        new Tweet(
                            array(
                                'id' => 3,
                                'username' => 'Test',
                                'text' => 'And a final random tweet',
                                'created_at' => '2015-01-01'
                            )
                        )
                    ),
                    'keywords' => array('random', 'tweet', 'a', 'final', 'And', 'Another', 'text', 'Some'),
                    'counts' => array(3,2,1,1,1,1,1,1)
                )
            )
        );
    }
}