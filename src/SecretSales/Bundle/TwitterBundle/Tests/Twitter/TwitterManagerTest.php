<?php

use SecretSales\Bundle\TwitterBundle\Twitter\TwitterManager;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class TwitterManagerTest extends KernelTestCase
{
    /**
     * @var TwitterManager
     */
    private $manager;

    public function setUp()
    {
        self::bootKernel();
        $this->manager = static::$kernel->getContainer()->get('secret_sales_twitter.twitter_manager');
    }

    /**
     * Tests that the service returns a valid object (TwitterResults)
     * @param $provider
     * @dataProvider getUsernameAndNumber
     * @group twitter
     */
    public function testSuccessfulFetch($provider)
    {
        $results = $this->manager->setMaxNumberOfTweets($provider['number'])
            ->fetch($provider['username']);

        $this->assertInstanceOf('SecretSales\Bundle\TwitterBundle\Twitter\TwitterResults', $results);

        $data = $results->getData();

        $this->assertEquals($provider['number'], count($data));

        foreach($data as $tweet) {
            $this->assertInstanceOf('Knp\Bundle\LastTweetsBundle\Twitter\Tweet', $tweet);
        }
    }

    /**
     * Tests that the service throws an exception when username is blank
     * @group twitter
     * @expectedException Exception
     */
    public function testBadFetch()
    {
        $this->manager->fetch('');
    }

    /**
     * Data provider to return normal dates
     * ArrayFetcher returns maximum 10 results.
     * @return array
     */
    static function getUsernameAndNumber()
    {
        return array(
            array('data' => array(
                    'username' => 'Secretsales',
                    'number' => 2
                )
            ),
            array('data' => array(
                    'username' => 'London',
                    'number' => 5
                )
            ),
            array('data' => array(
                    'username' => 'England',
                    'number' => 10
                )
            )
        );
    }
}