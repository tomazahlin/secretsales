<?php

use SecretSales\Bundle\TwitterBundle\Command\KeywordsCommand;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Tester\CommandTester;

class KeywordsCommandTest extends KernelTestCase
{
    /**
     * @var KeywordsCommand
     */
    private $command;

    /**
     * {@inheritDoc}
     */
    public function setUp()
    {
        $kernel = $this->createKernel();
        $kernel->boot();

        $command = new KeywordsCommand();

        $application = new Application($kernel);
        $application->add($command);

        $this->command = $application->find('secretsales:twitter:keywords');
    }

    /**
     * Tests that the command outputs keywords separated by comma and gets executed successfully
     * @group twitter
     */
    public function testSuccessfulExecute()
    {
        $commandTester = new CommandTester($this->command);
        $commandTester->execute(array('command' => $this->command->getName(), 'username' => 'Secretsales'));

        $this->assertGreaterThan(0, substr_count($commandTester->getDisplay(), ','));
        $this->assertEquals(0, $commandTester->getStatusCode());
    }

    /**
     * Tests that the command is not executed successfully when no username is passed
     * @group twitter
     * @expectedException RuntimeException
     */
    public function testNoUsernameExecute()
    {
        $commandTester = new CommandTester($this->command);
        $commandTester->execute(array('command' => $this->command->getName()));
    }

    /**
     * Tests that the command is not executed successfully when a bad number is entered
     * @group twitter
     * @expectedException InvalidArgumentException
     */
    public function testBadNumberExecute()
    {
        $commandTester = new CommandTester($this->command);
        $commandTester->execute(array('command' => $this->command->getName(), 'username' => 'Secretsales', 'number' => 'bad'));
    }
}