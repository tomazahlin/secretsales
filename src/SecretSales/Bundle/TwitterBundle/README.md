Secret Sales Twitter Bundle
=====================

This is the explanation of how the bundle is structured and also an installation tutorial for the bundle.

1) Installing vendors
---------------------

Use composer to install the vendors:

    composer install

Composer will install Symfony and all its dependencies, including dependencies that were used to help me complete this application.

2) Run tests
------------

Composer includes a dependency for phpunit.

To run the tests:

    phpunit -c app/
    
In test environment, the Array fetcher is used instead of OAuth fetcher, because tests should not rely on a working internet
connection, or any external web service, such as Twitter.


3) Running the command line utility
-----------------------------------

    php app/console secretsales:twitter:keywords username [--number=N]
    
4) Explanation of development
-----------------------------------

This bundle includes a command, which gets the latest tweets for a given username and outputs keywords, descending by their occurrence.

Commands are similar to Controllers, I would say they are both a part of UI layer of an application. For this reason, they should
contain as little code as possible, because their code is not easily reusable. The best scenario is to have the most of application covered
in service oriented architecture.

In this case I wrote the TwitterManager service, which gets injected the FetcherInterface and a default maximum number of tweets. Here I used an
interface injection, so for the testing, we can simply inject ArrayFetcher. The fetch method in TwitterManager class returns a TwitterResults
object, which I wrote separately, because the manipulation of the data should be in a separate class, that is so called single responsibility principle.
I have configured the TwitterManager to have prototype scope, because the manager allows to have modified number of tweets to fetch, so there is always
the predicted number of tweets when accessing the service multiple times in same request.

The biggest challenge for me was configuring the Inori/TwitterAppBundle and using TwitterOAuth library (not a bundle), because I had to find a
clean way to override it's default \TwitterOAuth class with the Abraham\TwitterOAuth. I did this in app/config/services.yml. Also, what you might
be interested in, is how I did the sorting of the keywords in TwitterResults class. I did not use any loops for that (which is a more clean approach),
instead I used some in-built php functions.

I also had to create a Twitter account to get the key and token for the Twitter API. If for some reason they might not work for you, please change
them to your own, in app/config/config.yml.
